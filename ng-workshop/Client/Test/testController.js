﻿app.controller('TestController', function ($scope) {
    $scope.tests = ['Zbynkuv test', 'Petruv test', 'Helenin test'];

    $scope.addTest = function (testName) {
        $scope.tests.push(testName);
        $scope.newTest = [];
    };
});